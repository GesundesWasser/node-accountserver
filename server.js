// Import necessary modules
const express = require('express');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');

// Initialize Express app
const app = express();
const PORT = process.env.PORT || 3000;

// Middleware to parse JSON bodies
app.use(bodyParser.json());

// Create MySQL connection
const connection = mysql.createConnection({
    host: '45.88.109.142',
    port: '45',
    user: 'user',
    password: 'milka4444!',
    database: 'dev'
});

// Connect to MySQL
connection.connect(err => {
    if (err) {
        console.error('Error connecting to MySQL: ', err);
        return;
    }
    console.log('Connected to MySQL');
});

// Route for user authentication (registration/login)
app.post('/auth', async (req, res) => {
    try {
        // Check if the action is for registration or login
        const action = req.body.action; // Assuming 'action' parameter specifies the action
        
        if (action === 'register') {
            // Registration logic
            const username = req.body.username;
            const password = req.body.password;
            
            // Check if the user already exists
            connection.query('SELECT * FROM users WHERE username = ?', [username], async (error, results) => {
                if (error) {
                    throw error;
                }
                if (results.length > 0) {
                    // User exists, return error
                    return res.status(409).send('User already exists');
                } else {
                    // Hash the password and insert the new user into the database
                    const hashedPassword = await bcrypt.hash(password, 10);
                    connection.query('INSERT INTO users (username, password) VALUES (?, ?)', [username, hashedPassword], (error, results) => {
                        if (error) {
                            throw error;
                        }
                        // User registered successfully
                        res.status(201).send('User registered successfully');
                    });
                }
            });
        } else if (action === 'login') {
            // Login logic
            const username = req.body.username;
            const password = req.body.password;
            
            // Find the user by username in the database
            connection.query('SELECT * FROM users WHERE username = ?', [username], async (error, results) => {
                if (error) {
                    throw error;
                }
                if (results.length === 0) {
                    // User not found, return error
                    return res.status(404).send('User not found');
                } else {
                    // Compare passwords
                    const user = results[0];
                    if (await bcrypt.compare(password, user.password)) {
                        // Passwords match, login successful
                        res.send('Login successful');
                    } else {
                        // Passwords don't match, return error
                        res.status(401).send('Incorrect password');
                    }
                }
            });
        } else {
            // Invalid action, return error
            res.status(400).send('Invalid action');
        }
    } catch {
        res.status(500).send('Error authenticating/registering user');
    }
});

// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});